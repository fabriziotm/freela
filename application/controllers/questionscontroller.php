<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class QuestionsController extends CI_Controller {

	public function index()
	{
		$this->load->model('Questionmodel','', TRUE);	
	    $list = $this->Questionmodel->getList();

		$data['title'] = "Questions";
		$data['questions'] = $list;


	    $this->load->view('main',$data);

	}

}