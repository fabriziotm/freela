<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class VerifyLogin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Loginmodel','',TRUE);
	}

	public function index(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

		if($this->form_validation->run() == FALSE){
			$data['title'] = 'Login';
			$this->load->view('perklog/login/main',$data);
		}else{
			redirect('index', 'refresh');
		}

	}

	public function check_database($password){
		$email = $this->input->post('email');

		$result = $this->Loginmodel->login($email, $password);

		if($result){
			$sess_array = array();
			foreach($result as $row){
				$sess_array = array(
						'idUser' => $row->idUser,
						'email' => $row->email
				);
				$this->session->set_userdata('logged_in', $sess_array);
			}
			return TRUE;
		}else{
			$this->form_validation->set_message('check_database', 'Invalid Email or Password');
			return false;
		}
	}

	public function logout(){
	   $this->session->unset_userdata('logged_in');
	   $this->session->sess_destroy();
	   redirect('login', 'refresh');
	 }
}