<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<title><?php echo $title ?></title>	
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	
		<?php  echo link_tag('css/util/default.css'); ?>
		<?php  echo link_tag('css/util/multilevelmenu.css'); ?>
		<?php  echo link_tag('css/util/component.css'); ?>
		<?php  echo link_tag('css/util/animations.css'); ?>

		<script src="<?php echo base_url();?>js/util/modernizr.custom.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>js/util/jquery-1.10.2.js" ></script>
		<script type="text/javascript" src="<?php echo base_url();?>js/util/jquery-ui/jquery-ui.js" ></script>
		<script type="text/javascript" src="<?php echo base_url();?>js/questions/answer.js" ></script>	
	</head>
	<body>	
		<div id="container">

			<div class="pt-triggers">
				<div id="start" class="pt-touch-button hvr-bubble-bottom">Start</div>
				<div id="next" class="pt-touch-button hvr-bubble-right hide">Next Question</div>
				<div id="back" class="pt-touch-button hvr-bubble-left hide">Previous Question</div>
				<div id="result" class="pt-touch-button hvr-bubble-bottom hide">Result</div>
				<div id="nothx" class="pt-touch-button hvr-shrink hide">No, Thanks.</div>
				<div id="send" class="pt-touch-button hvr-buzz hide">Send</div>
			</div><!-- /triggers -->

			<div id="pt-main" class="pt-perspective">
				<div class="pt-page pt-page-1">
					<div class="label">Welcome!</div>
					<?php
						//Role and industry
					?>
				</div>
				<?php
					$i = 2;
					foreach ($questions as $question) 
					{
						echo '<div class="pt-page pt-page-'.$i.'">';
						echo "<div class='label'>".($i-1)./*". ".$question->label.*/"</div>";
						echo '<table class="table">';
						if($question->question_type_id == 2){
							echo    '<col width="20%"><col width="20%"><col width="20%"><col width="20%"><col width="20%">';
							//echo	'<tr>';
							//echo 		'<td>Strongly disagree</td>';
							//echo 		'<td>Disagree</td>';
							//echo 		'<td>Neutral</td>';
							//echo 		'<td>Agree</td>';
							//echo 		'<td>Strongly agree</td>';
							//echo 	'</tr>';
							//echo 	'<tr>';
							//echo 		"<td><input type='radio' name='".$i."' value='1'/></td>";
							//echo 		"<td><input type='radio' name='".$i."' value='2'/></td>";
							//echo 		"<td><input type='radio' name='".$i."' value='3'/></td>";
							//echo 		"<td><input type='radio' name='".$i."' value='4'/></td>";
							//echo 		"<td><input type='radio' name='".$i."' value='5'/></td>";
							//echo 	"</tr>";
							echo 	'<tr>';
							echo 		'<td colspan="5">';
							echo 		'<div id="sd" class="likert l'.$i.'"></div><div id="d" class="likert l'.$i.'"></div><div id="n" class="likert l'.$i.'"></div><div id="a" class="likert l'.$i.'"></div><div id="sa" class="likert l'.$i.'"></div>';	
							echo 		'</td>';
							echo 	'</tr>';
						}else{
							//echo    '<col width="50%"><col width="50%">';
							//echo	'<tr>';
							//echo 		'<td>Yes</td>';
							//echo 		'<td>No</td>';
							//echo 	'</tr>';
							//echo 	"<tr>";
							//echo 		"<td><input type='radio' name='".$i."' value='6'/></td>";
							//echo 		"<td><input type='radio' name='".$i."' value='7'/></td>";
							//echo 	"</tr>";
						}
						echo '</table>';
						echo '</div>';
						$i++;
					}

				?>
				<div class="pt-page pt-page-14">
					<div class="label">Info</div>
				</div>
				<div class="pt-page pt-page-15">
					<div class="label">Result!</div>
				</div>
			</div>

			<div class="pt-message">
				<p>Your browser does not support CSS animations.</p>
			</div>

			<div id="steps">
				<div id="step-1" class="step">
					<h2> Start the &nbspSurvey</h2>
					<p>Click on Start button, tell us about your role and industry and start the survey. It's simple, you'll see!</p>	
				</div>
				<div id="step-2" class="step">
					<h2> Answer the Questions</h2>
					<p>There are only 12 simple questions, divided in 2 types. You can answer them by filling the concordance bar or choosing Yes / No.</p>					
				</div>
				<div id="step-3" class="step">
					<h2> Give us your Details</h2>
					<p>With more details and your info we can contact you and discuss more ways to help you.</p>
				</div>
				<div id="step-4" class="step">
					<h2> Get your Result</h2>
					<p>See our fist view about your case. If you've given us your details we'll contact you soon.</p>
				</div>
			</div>

			<script src="<?php echo base_url();?>js/util/jquery.dlmenu.js"></script>
			<script src="<?php echo base_url();?>js/util/pagetransitions.js"></script>
		</div>
	</body>
</html>
