<?php  echo link_tag('css/questions/questions.css'); ?>
<script type="text/javascript" src="<?php echo base_url();?>js/questions/questions.js" ></script>

<div id="count"></div>

<div id="questionsDiv">
	
<?php
	$i = 1;
	foreach ($questions as $question) 
	{
		echo '<div id="q'.$i.'" class="div">';
			echo "<div class='label'><h2>".$i.". ".$question->label."</h2></div>";
			echo '<table class="table">';
			if($question->question_type_id == 2){
				echo    '<col width="20%"><col width="20%"><col width="20%"><col width="20%"><col width="20%">';
				echo	'<tr>';
				echo 		'<td>Strongly disagree</td>';
				echo 		'<td>Disagree</td>';
				echo 		'<td>Neutral</td>';
				echo 		'<td>Agree</td>';
				echo 		'<td>Strongly agree</td>';
				echo 	'</tr>';
				echo 	'<tr>';
				echo 		"<td><input type='radio' name='".$i."' value='1'/></td>";
				echo 		"<td><input type='radio' name='".$i."' value='2'/></td>";
				echo 		"<td><input type='radio' name='".$i."' value='3'/></td>";
				echo 		"<td><input type='radio' name='".$i."' value='4'/></td>";
				echo 		"<td><input type='radio' name='".$i."' value='5'/></td>";
				echo 	"</tr>";
			}else{
				echo    '<col width="50%"><col width="50%">';
				echo 	"<tr>";
				echo 		"<td colspan='2'><div class='question'>".$question->label."</div></td>";
				echo 	"</tr>";
				echo	'<tr>';
				echo 		'<td>Yes</td>';
				echo 		'<td>No</td>';
				echo 	'</tr>';
				echo 	"<tr>";
				echo 		"<td><input type='radio' name='".$i."' value='6'/></td>";
				echo 		"<td><input type='radio' name='".$i."' value='7'/></td>";
				echo 	"</tr>";
			}
			echo '</table>';
		echo '</div>';
		$i++;
	}
?>
</div>

	<div id="back">Back</div>
	<div id="next">Next</div>
	<div id="text">
		Organization's Industry: <input type="text"><br>
		Your role: <input type="text"><br>
		Tell us more about yourself and your company<br>
		<textarea id="textarea" maxlength="300"></textarea></div>

<div id="resultDiv"></div>

<div id="result">Results</div>

<div id="login">Login</div>

<div id="loginform" title="Login">
	<div id="alerts">
		<?php echo validation_errors(); ?>	
	</div>
	<div id="formDiv">
		<?php echo form_open('verifyLogin'); ?>
			<table>
				<tr>
					<td>
						<div id="email">
							<input type="text" class="email" name="email" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'" value="<?php echo set_value('email'); ?>">
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div id="password">
							<input type="password" class="password" name="password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">
						</div>
					</td>
				</tr>
			</table>   			
  		</form>
  	</div>
</div>