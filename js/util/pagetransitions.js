var PageTransitions = (function() {

	var $main = $( '#pt-main' ),
		$pages = $main.children( 'div.pt-page' ),
		animcursor = 1,
		pagesCount = $pages.length,
		current = 0,
		isAnimating = false,
		endCurrPage = false,
		endNextPage = false,
		animEndEventNames = {
			'WebkitAnimation' : 'webkitAnimationEnd',
			'OAnimation' : 'oAnimationEnd',
			'msAnimation' : 'MSAnimationEnd',
			'animation' : 'animationend'
		},
		// animation end event name
		animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
		// support css animations
		support = Modernizr.cssanimations;
	
	function init() {

		$pages.each( function() {
			var $page = $( this );
			$page.data( 'originalClassList', $page.attr( 'class' ) );
		} );

		$pages.eq( current ).addClass( 'pt-page-current' );

		$( '#dl-menu' ).dlmenu( {
			animationClasses : { in : 'dl-animate-in-2', out : 'dl-animate-out-2' },
			onLinkClick : function( el, ev ) {
				ev.preventDefault();
				nextPage( el.data( 'animation' ) );
			}
		} );

		$( '#next' ).on( 'click', function() {
			if( isAnimating ) {
				return false;
			}
			if(current != 12)
				nextPage( 1 );

		} );

		$( '#back' ).on( 'click', function() {
			if( isAnimating ) {
				return false;
			}
			if(current > 1)
				previousPage( 1 );

		} );

		$( '#start' ).on( 'click', function() {
			if( isAnimating ) {
				return false;
			}
			nextPage( 1 );
			$(this).addClass('hide');
			$('#next').removeClass('hide');
		} );

		$( '#result' ).on( 'click', function() {
			if( isAnimating ) {
				return false;
			}
			nextPage( 1 );

		} );

		$( '#nothx' ).on( 'click', function() {
			if( isAnimating ) {
				return false;
			}
			nextPage( 1 );

		} );

		$( '#send' ).on( 'click', function() {
			if( isAnimating ) {
				return false;
			}
			nextPage( 1 );

		} );

	}

	function start(options ) {
		var animation = (options.animation) ? options.animation : options;

		if( isAnimating ) {
			return false;
		}

		isAnimating = true;
		
		var $currPage = $pages.eq( current );

		++current;

		var $nextPage = $pages.eq( current ).addClass( 'pt-page-current' ), outClass = '', inClass = '';
		outClass = 'pt-page-moveToTop';
		inClass = 'pt-page-moveFromBottom';


		$currPage.addClass( outClass ).on( animEndEventName, function() {
			$currPage.off( animEndEventName );
			endCurrPage = true;
			if( endNextPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		$nextPage.addClass( inClass ).on( animEndEventName, function() {
			$nextPage.off( animEndEventName );
			endNextPage = true;
			if( endCurrPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		if( !support ) {
			onEndAnimation( $currPage, $nextPage );
		}

	}

	function nextPage(options ) {
		var animation = (options.animation) ? options.animation : options;

		if( isAnimating ) {
			return false;
		}

		isAnimating = true;
		
		var $currPage = $pages.eq( current );

		if(options.showPage){
			if( options.showPage < pagesCount - 1 ) {
				current = options.showPage;
			}
			else {
				current = 0;
			}
		}
		else{
			if( current < pagesCount - 1 ) {
				++current;
			}
			else {
				current = 0;
			}
		}
		switch(current){
			case 2:
				$('#back').removeClass('hide');
				break;
			case 12:
				$('#next').addClass('hide');
				$('#result').removeClass('hide');
				break;
			case 13:
				$('#result').addClass('hide');
				$('#back').addClass('hide');
				$('#nothx').removeClass('hide');
				$('#send').removeClass('hide');
				break;
			case 14:
				$('#nothx').addClass('hide');
				$('#send').addClass('hide');
				break;
		}

		var $nextPage = $pages.eq( current ).addClass( 'pt-page-current' ), outClass = '', inClass = '';
		switch(current){
			case 1:
			case 13:
				outClass = 'pt-page-moveToTop';
				inClass = 'pt-page-moveFromBottom';
				break;
			default:
				outClass = 'pt-page-moveToLeft';
				inClass = 'pt-page-moveFromRight';
				break;
		}


		$currPage.addClass( outClass ).on( animEndEventName, function() {
			$currPage.off( animEndEventName );
			endCurrPage = true;
			if( endNextPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		$nextPage.addClass( inClass ).on( animEndEventName, function() {
			$nextPage.off( animEndEventName );
			endNextPage = true;
			if( endCurrPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		if( !support ) {
			onEndAnimation( $currPage, $nextPage );
		}

	}

	function previousPage(options ) {
		var animation = (options.animation) ? options.animation : options;

		if( isAnimating ) {
			return false;
		}

		isAnimating = true;
		
		var $currPage = $pages.eq( current );

		if(options.showPage){
			if( options.showPage < pagesCount - 1 ) {
				current = options.showPage;
			}
			else {
				current = 0;
			}
		}
		else{
			if( current < pagesCount - 1 ) {
				--current;
			}
			else {
				current = 0;
			}
		}

		if(current == 1){
			$('#back').addClass('hide');
		}else if(current == 10){
			$('#next').removeClass('hide');
		}
		var $nextPage = $pages.eq( current ).addClass( 'pt-page-current' ), outClass = '', inClass = '';

		outClass = 'pt-page-moveToRight';
		inClass = 'pt-page-moveFromLeft';

		$nextPage.addClass( inClass ).on( animEndEventName, function() {
			$nextPage.off( animEndEventName );
			endNextPage = true;
			if( endCurrPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		$currPage.addClass( outClass ).on( animEndEventName, function() {
			$currPage.off( animEndEventName );
			endCurrPage = true;
			if( endNextPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		

		if( !support ) {
			onEndAnimation( $currPage, $nextPage );
		}

	}

	function onEndAnimation( $outpage, $inpage ) {
		endCurrPage = false;
		endNextPage = false;
		resetPage( $outpage, $inpage );
		isAnimating = false;
	}

	function resetPage( $outpage, $inpage ) {
		$outpage.attr( 'class', $outpage.data( 'originalClassList' ) );
		$inpage.attr( 'class', $inpage.data( 'originalClassList' ) + ' pt-page-current' );
	}

	init();

	return { 
		init : init,
		nextPage : nextPage,
	};

})();