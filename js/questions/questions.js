var page = 1;
$(document).ready(function(){
	$("body").disableSelection();

	dialog = $( "#loginform" ).dialog({
      autoOpen: false,
      height: 300,
      width: 300,
      modal: true,
      buttons: {
        Cancel: function() {
          dialog.dialog( "close" );
        }
      }
    });

	start();

	$('#result').click(function(){
		$('#resultDiv').text(getResult());
		$('#resultDiv').removeClass('hide');
		$('#questionsDiv').addClass('hide');
	});

	$('#back').click(function(){
		var question = $('.show').attr('id');
		var num = question.substring(1);
	
		switch(num){
			case '2':
				$('#q2').removeClass('show');
				$('#q2').addClass('hide');
				$('#q1').removeClass('hide');
				$('#q1').addClass('show');
				$('#back').addClass('hide');
				$('#count').text('1/12');
				break;
			case '3':
				$('#q3').removeClass('show');
				$('#q3').addClass('hide');
				$('#q2').removeClass('hide');
				$('#q2').addClass('show');
				$('#count').text('2/12');
				break;
			case '4':
				$('#q4').removeClass('show');
				$('#q4').addClass('hide');
				$('#q3').removeClass('hide');
				$('#q3').addClass('show');
				$('#count').text('3/12');
				break;
			case '5':
				$('#q5').removeClass('show');
				$('#q5').addClass('hide');
				$('#q4').removeClass('hide');
				$('#q4').addClass('show');
				$('#count').text('4/12');
				break;
			case '6':
				$('#q6').removeClass('show');
				$('#q6').addClass('hide');
				$('#q5').removeClass('hide');
				$('#q5').addClass('show');
				$('#count').text('5/12');
				break;
			case '7':
				$('#q7').removeClass('show');
				$('#q7').addClass('hide');
				$('#q6').removeClass('hide');
				$('#q6').addClass('show');
				$('#count').text('6/12');
				break;
			case '8':
				$('#q8').removeClass('show');
				$('#q8').addClass('hide');
				$('#q7').removeClass('hide');
				$('#q7').addClass('show');
				$('#count').text('7/12');
				break;
			case '9':
				$('#q9').removeClass('show');
				$('#q9').addClass('hide');
				$('#q8').removeClass('hide');
				$('#q8').addClass('show');
				$('#count').text('8/12');
				break;
			case '10':
				$('#q10').removeClass('show');
				$('#q10').addClass('hide');
				$('#q9').removeClass('hide');
				$('#q9').addClass('show');
				$('#count').text('9/12');
				break;
			case '11':
				$('#q11').removeClass('show');
				$('#q11').addClass('hide');
				$('#q10').removeClass('hide');
				$('#q10').addClass('show');
				$('#count').text('10/12');
				break;
			case '12':
				$('#q12').removeClass('show');
				$('#q12').addClass('hide');
				$('#q11').removeClass('hide');
				$('#q11').addClass('show');
				$('#next').removeClass('hide');
				$('#count').text('11/12');
				break;
		}
	});

	$('#next').click(function(){

		var question = $('.show').attr('id');
		var num = question.substring(1);

		if($('input[name='+num+']:checked').val()){
			switch(num){
				case '1':
					$('#q1').removeClass('show');
					$('#q1').addClass('hide');
					$('#q2').removeClass('hide');
					$('#q2').addClass('show');
					$('#back').removeClass('hide');
					$('#count').text('2/12');
					break;
				case '2':
					$('#q2').removeClass('show');
					$('#q2').addClass('hide');
					$('#q3').removeClass('hide');
					$('#q3').addClass('show');
					$('#count').text('3/12');
					break;
				case '3':
					$('#q3').removeClass('show');
					$('#q3').addClass('hide');
					$('#q4').removeClass('hide');
					$('#q4').addClass('show');
					$('#count').text('4/12');
					break;
				case '4':
					$('#q4').removeClass('show');
					$('#q4').addClass('hide');
					$('#q5').removeClass('hide');
					$('#q5').addClass('show');
					$('#count').text('5/12');
					break;
				case '5':
					$('#q5').removeClass('show');
					$('#q5').addClass('hide');
					$('#q6').removeClass('hide');
					$('#q6').addClass('show');
					$('#count').text('6/12');
					break;
				case '6':
					$('#q6').removeClass('show');
					$('#q6').addClass('hide');
					$('#q7').removeClass('hide');
					$('#q7').addClass('show');
					$('#count').text('7/12');
					break;
				case '7':
					$('#q7').removeClass('show');
					$('#q7').addClass('hide');
					$('#q8').removeClass('hide');
					$('#q8').addClass('show');
					$('#count').text('8/12');
					break;
				case '8':
					$('#q8').removeClass('show');
					$('#q8').addClass('hide');
					$('#q9').removeClass('hide');
					$('#q9').addClass('show');
					$('#count').text('9/12');
					break;
				case '9':
					$('#q9').removeClass('show');
					$('#q9').addClass('hide');
					$('#q10').removeClass('hide');
					$('#q10').addClass('show');
					$('#count').text('10/12');
					break;
				case '10':
					$('#q10').removeClass('show');
					$('#q10').addClass('hide');
					$('#q11').removeClass('hide');
					$('#q11').addClass('show');
					$('#count').text('11/12');
					break;
				case '11':
					$('#q11').removeClass('show');
					$('#q11').addClass('hide');
					$('#q12').removeClass('hide');
					$('#q12').addClass('show');
					$('#next').addClass('hide');
					$('#count').text('12/12');
					break;
			}
		}else{
			switch(num){
				case '1':
					$('#q1').removeClass('show');
					$('#q1').addClass('hide');
					$('#q2').removeClass('hide');
					$('#q2').addClass('show');
					$('#back').removeClass('hide');
					$('#count').text('2/12');
					break;
				case '2':
					$('#q2').removeClass('show');
					$('#q2').addClass('hide');
					$('#q3').removeClass('hide');
					$('#q3').addClass('show');
					$('#count').text('3/12');
					break;
				case '3':
					$('#q3').removeClass('show');
					$('#q3').addClass('hide');
					$('#q4').removeClass('hide');
					$('#q4').addClass('show');
					$('#count').text('4/12');
					break;
				case '4':
					$('#q4').removeClass('show');
					$('#q4').addClass('hide');
					$('#q5').removeClass('hide');
					$('#q5').addClass('show');
					$('#count').text('5/12');
					break;
				case '5':
					$('#q5').removeClass('show');
					$('#q5').addClass('hide');
					$('#q6').removeClass('hide');
					$('#q6').addClass('show');
					$('#count').text('6/12');
					break;
				case '6':
					$('#q6').removeClass('show');
					$('#q6').addClass('hide');
					$('#q7').removeClass('hide');
					$('#q7').addClass('show');
					$('#count').text('7/12');
					break;
				case '7':
					$('#q7').removeClass('show');
					$('#q7').addClass('hide');
					$('#q8').removeClass('hide');
					$('#q8').addClass('show');
					$('#count').text('8/12');
					break;
				case '8':
					$('#q8').removeClass('show');
					$('#q8').addClass('hide');
					$('#q9').removeClass('hide');
					$('#q9').addClass('show');
					$('#count').text('9/12');
					break;
				case '9':
					$('#q9').removeClass('show');
					$('#q9').addClass('hide');
					$('#q10').removeClass('hide');
					$('#q10').addClass('show');
					$('#count').text('10/12');
					break;
				case '10':
					$('#q10').removeClass('show');
					$('#q10').addClass('hide');
					$('#q11').removeClass('hide');
					$('#q11').addClass('show');
					$('#count').text('11/12');
					break;
				case '11':
					$('#q11').removeClass('show');
					$('#q11').addClass('hide');
					$('#q12').removeClass('hide');
					$('#q12').addClass('show');
					$('#next').addClass('hide');
					$('#count').text('12/12');
					break;
			}
		}
	});

	$('#login').click(function(){
        dialog.dialog("open");
    });
});

function getResult(){
	var result = 0;

	for (var i = 1; i <= 12; i++) {
		if($('input[name='+i+']:checked').val())
			result += parseInt($('input[name='+i+']:checked').val());
	};
	
	return result;
}

function start(){
	$('#count').removeClass('hide');
	$('#count').text('1/12');

	$('#questionsDiv').removeClass('hide');
	$('#resultDiv').addClass('hide');

	$('.div').addClass('hide');
	$('#q1').removeClass('hide');
	$('#q1').addClass('show');

	$('#back').addClass('hide');
	$('#next').removeClass('hide');
}