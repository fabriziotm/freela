$(document).ready(function() {
  $('.likert').hover(
    function(){
      var answer = $(this).attr('id');
      var question = $(this).attr('class');
      question = question.substr(8,2);

      switch(answer){
        case 'sd':
          if($('#sd.l'+question).hasClass('chosen')){
            $('#sd.l'+question).addClass('highlightsub');
          }else{
            $('#sd.l'+question).addClass('highlight');
          }
          break;
        case 'd':
          if($('#d.l'+question).hasClass('chosen')){
            $('#sd.l'+question).addClass('highlightsub');
            $('#d.l'+question).addClass('highlightsub');
          }else{
            $('#sd.l'+question).addClass('highlight');
            $('#d.l'+question).addClass('highlight');
          }
          break;
        case 'n':
          if($('#n.l'+question).hasClass('chosen')){
            $('#sd.l'+question).addClass('highlightsub');
            $('#d.l'+question).addClass('highlightsub');
            $('#n.l'+question).addClass('highlightsub');
          }else{
            $('#sd.l'+question).addClass('highlight');
            $('#d.l'+question).addClass('highlight');
            $('#n.l'+question).addClass('highlight');
          }
          break;
        case 'a':
          if($('#a.l'+question).hasClass('chosen')){
            $('#sd.l'+question).addClass('highlightsub');
            $('#d.l'+question).addClass('highlightsub');
            $('#n.l'+question).addClass('highlightsub');
            $('#a.l'+question).addClass('highlightsub');
          }else{
            $('#sd.l'+question).addClass('highlight');
            $('#d.l'+question).addClass('highlight');
            $('#n.l'+question).addClass('highlight');
            $('#a.l'+question).addClass('highlight');
          }
          break;
        case 'sa':
          if($('#sa.l'+question).hasClass('chosen')){
            $('#sd.l'+question).addClass('highlightsub');
            $('#d.l'+question).addClass('highlightsub');
            $('#n.l'+question).addClass('highlightsub');
            $('#a.l'+question).addClass('highlightsub');
            $('#sa.l'+question).addClass('highlightsub');
          }else{
            $('#sd.l'+question).addClass('highlight');
            $('#d.l'+question).addClass('highlight');
            $('#n.l'+question).addClass('highlight');
            $('#a.l'+question).addClass('highlight');
            $('#sa.l'+question).addClass('highlight');
          }
          break;
      }
    },
    function(){
          var question = $(this).attr('class');
          question = question.substr(8,2);

          $('#sd.l'+question).removeClass('highlight');
          $('#d.l'+question).removeClass('highlight');
          $('#n.l'+question).removeClass('highlight');
          $('#a.l'+question).removeClass('highlight');
          $('#sa.l'+question).removeClass('highlight');

          $('#sd.l'+question).removeClass('highlightsub');
          $('#d.l'+question).removeClass('highlightsub');
          $('#n.l'+question).removeClass('highlightsub');
          $('#a.l'+question).removeClass('highlightsub');
          $('#sa.l'+question).removeClass('highlightsub');
    }
  );

  $('.likert').click(function(){
  	var answer = $(this).attr('id');
  	var question = $(this).attr('class');
  	question = question.substr(8,2);

  	resetAnswer(question);

  	switch(answer){
  		case 'sd':
  			$('#sd.l'+question).addClass('chosen');
  			break;
  		case 'd':
  			$('#sd.l'+question).addClass('chosen');
  			$('#d.l'+question).addClass('chosen');
  			break;
  		case 'n':
  			$('#sd.l'+question).addClass('chosen');
  			$('#d.l'+question).addClass('chosen');
  			$('#n.l'+question).addClass('chosen');
  			break;
  		case 'a':
  			$('#sd.l'+question).addClass('chosen');
  			$('#d.l'+question).addClass('chosen');
  			$('#n.l'+question).addClass('chosen');
  			$('#a.l'+question).addClass('chosen');
  			break;
  		case 'sa':
  			$('#sd.l'+question).addClass('chosen');
  			$('#d.l'+question).addClass('chosen');
  			$('#n.l'+question).addClass('chosen');
  			$('#a.l'+question).addClass('chosen');
  			$('#sa.l'+question).addClass('chosen');
  			break;
  	}
  });
});

function resetAnswer(e){
	$(function(){
		$('.l'+e).removeClass('chosen');
	});
	
}